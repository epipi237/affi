<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class affiliate extends Model
{
    public function affiliatemetas(){
    	return $this->hasMany('App\affiliatemeta');
    }
    public function referrals(){
    	return $this->hasMany('App\referral');
    }
    public function payouts(){
    	return $this->hasMany('App\payout');
    }
    public function User(){
        return $this->hasOne('App\User');
    }
}
