<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('massketting.particles.index');
});
Route::post('/pending','Auth\RegisterController@registerPending')->name('pending');

Auth::routes();


Route::get('/logout', 'HomeController@logout')->name('logout');

Route::prefix('home')->middleware(['auth'])->group(function() {
Route::get('/', 'HomeController@index')->name('home');
Route::get('/approval','HomeController@approveAccount');

});

