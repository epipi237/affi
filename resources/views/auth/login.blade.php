@extends('layouts.auth')

@section('content')

<div class="login-panel panel panel-default">
    <div class="panel-heading">Log in</div>
    <div class="panel-body">
        <form role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <fieldset>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="checkbox">
                    <label>
                        <input name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>Remember Me
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">
                    Login
                </button>

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
            </fieldset>
        </form>
        <span>
            Don't yet have an account?
            <a class="btn btn-link" href="{{ route('register') }}">Register</a>
        </span>
    </div>
</div>
@endsection
